"""Stream type classes for Criteo version 2021.04."""

from datetime import datetime, timedelta
from pathlib import Path
from typing import cast

from dateutil.parser import parse

from singer_sdk import typing as th
from singer_sdk.plugin_base import PluginBase as TapBaseClass

from tap_criteo.client import CriteoSearchStream, CriteoStream
from tap_criteo.streams.reports import analytics_type_mappings, value_func_mapping, dictConversion

import psycopg2, pendulum, json

import logging
LOGGER = logging.getLogger(__name__)

SCHEMAS_DIR = Path(__file__).parent.parent / "./schemas"

from datetime import datetime
from decimal import Decimal
from typing import Any, Callable, Dict


import logging
from datetime import datetime
from typing import Any, Callable, Dict, Iterable, List, Optional, Union, cast


class AudiencesStream(CriteoStream):
    """Audiences stream."""

    name = "audiences"
    path = "/2021-04/audiences"
    schema_filepath = SCHEMAS_DIR / "audience.json"


class AdvertisersStream(CriteoStream):
    """Advertisers stream."""

    name = "advertisers"
    path = "/2021-04/advertisers/me"
    schema_filepath = SCHEMAS_DIR / "advertiser.json"


class AdSetsStream(CriteoSearchStream):
    """Ad sets stream."""

    name = "ad_sets"
    path = "/2021-04/marketing-solutions/ad-sets/search"
    schema_filepath = SCHEMAS_DIR / "ad_set.json"


class StatsReportStream(CriteoStream):
    """Statistics reports stream."""

    name = "statistics"
    path = "/2021-04/statistics/report"
    records_jsonpath = "$.Rows[*]"
    rest_method = "post"

    def __init__(
        self,
        tap: TapBaseClass,
        report: dict,
    ):
        name = report["name"]
        replicationKeyName = report["replication_key"]["name"]
        replicationKeyType = report["replication_key"]["type"]

        # Handle empty replicationKeyType["format"]
        replicationKeyTypeDict = {}
        replicationKeyTypeDict["type"] = replicationKeyType["type"]
        if replicationKeyType["format"]:
            replicationKeyTypeDict["format"] = replicationKeyType["format"]
        schema = {"properties": {replicationKeyName: replicationKeyTypeDict, "Currency": {"type": "string"}}}


        schema["properties"].update({k: analytics_type_mappings[k] for k in report["metrics"]})
        schema["properties"].update({k: analytics_type_mappings[k] for k in report["dimensions"]})

        super().__init__(tap, name=name, schema=schema)

        self.currency = report["currency"]
        self.format = report["format"]
        self.timezone = report["timezone"]
        self.bookmark = report["bookmark"]
        self.dimensions = report["dimensions"]
        self.metrics = report["metrics"]
        # self.primary_keys = self.dimensions
        self.start_date = self.config["start_date"]
        self.end_date = self.config["end_date"]
        self.replication_key = replicationKeyName
        self.replication_end_date = ''
        # self.replication_end_date = datetime.strptime(self.config["end_date"], '%Y-%m-%dT%H:%M:%SZ').date()

    # Override
    def get_records(self, context: Optional[dict]) -> Iterable[Dict[str, Any]]:
        # NOTE: Generators are awesome, but we need to handle the case where the result is empty to keep the incremental (based on the DAY field) up to date and prevent the replication method getting stuck
        listRecords = list(self.request_records(context))

        # Empty response -> re-create it with empty fields except for the replication key (Day)
        if not listRecords:
            simulated_record = dict.fromkeys(self.dimensions + self.metrics, '') # NOTE: temporary 
            transformed_record = self.post_process_empty(simulated_record, context)
            # Adding the replication key (Day)
            transformed_record[self.replication_key] = self.replication_end_date.strftime("%Y-%m-%d")

            # Fake response, exit immediatly
            yield transformed_record
        else:
            for record in listRecords:
                transformed_record = self.post_process(record, context)
                if transformed_record is None:
                    # Record filtered out during post_process()
                    continue
                transformed_record[self.replication_key] = self.replication_end_date.strftime("%Y-%m-%d")
                yield transformed_record

    def prepare_request_payload(self, context, next_page_token) -> dict:
        last_updated_after = ''
        dayStart = parse(self.config["start_date"])
        dayEnd = parse(self.config["end_date"])
        
        if self.config['use_remote_date'] == 1:
            # Trasformazioni su last_updated_after: start: sottraggo un tot di giorni, end: aggiungo un giorno
            last_updated_after = self.getLastReplicationKey(context)
            dayStart = last_updated_after - timedelta(days=self.config['time_delta'])
            dayEnd = last_updated_after + timedelta(days=1)
            # LOGGER.warning("==========================================")
            # LOGGER.warning(last_updated_after)
            # LOGGER.warning(dayStart)
            # LOGGER.warning(dayEnd)
            # LOGGER.warning("==========================================")

        # Patch: "The start date can not be more than 2 years in the past."
        daysToSubtract = (2 * 365) - int(self.config['time_delta']) - 7 # NOTE: Calculate 2 years, minus the delta, minus 7, the magic number
        date2YearsOld = datetime.utcnow() - timedelta(days=daysToSubtract)
        if dayStart.isoformat() < date2YearsOld.isoformat():
            dayStartNew = date2YearsOld
            dayEndNew = dayEnd
            dateEndChanged = False

            if dayEnd.isoformat() < date2YearsOld.isoformat():
                dayEndNew = dayStartNew + timedelta(days=1)
                dateEndChanged = True
            
            LOGGER.warning("------------------------------------------")
            LOGGER.warning("== DEVELOPER NOTE ==")
            LOGGER.warning("API constraint: The start date can not be more than 2 years in the past")
            LOGGER.warning(f"Changing startDate from '{dayStart}' to '{dayStartNew}'")
            if dateEndChanged:
                LOGGER.warning(f"Changing endDate from '{dayEnd}' to '{dayEndNew}'")
            LOGGER.warning("------------------------------------------")
            dayStart = dayStartNew
            dayEnd = dayEndNew
        
        # Updating the configuration with the replication key
        self.replication_end_date = dayEnd

        # quit("\n\n ================ THE END ================= \n\n")


        return {
            "dimensions": self.dimensions,
            "metrics": self.metrics,
            "currency": self.currency,
            "format": self.format,
            "timezone": self.timezone,
            "startDate": dayStart.isoformat(),
            "endDate": dayEnd.isoformat(),
        }


    def post_process(self, row: dict, context) -> dict:
        for key in row:
            func = value_func_mapping.get(key)
            if func:
                row[key] = func(row[key])
        return row

    # Assign to each dictionary key the corrisponding empty value ("", 0, 0.00, and so on)
    def post_process_empty(self, row: dict, context) -> dict:
        for key in row:
            func = value_func_mapping.get(key)
            if func:
                row[key] = func(dictConversion[key]())
        return row


    def getLastReplicationKey(self,context):

        # NOTE: potential different replication formats -> getting the correct one
        replication_key_orig = self.get_starting_replication_key_value(context)
        replication_key = self.start_date

        if replication_key_orig:
            try:
                replication_key = datetime.strptime(replication_key_orig, '%Y-%m-%dT%H:%M:%SZ')
            except ValueError:
                replication_key = datetime.strptime(replication_key_orig, '%Y-%m-%d')
        
        # replication_key = cast(datetime, pendulum.parse(replication_key))

        # LOGGER.warning("++++++++++++++++++++++++++++++++++++++++++")
        # LOGGER.warning(replication_key_orig)
        # LOGGER.warning(replication_key)
        # LOGGER.warning(type(replication_key))
        # LOGGER.warning("------------------------------------------")

        jobId = self.config["jobId"]
        bookmark = self.bookmark
        replication_key = self.getLastSuccessfulRunReplicationKey(jobId=jobId, bookmark=bookmark, start_date=replication_key)

        # Trasformazioni su start_date: aggiungo un secondo
        # replication_key = replication_key + timedelta(seconds=1)

        # LOGGER.warning(replication_key)
        # LOGGER.warning(type(replication_key))
        # LOGGER.warning("++++++++++++++++++++++++++++++++++++++++++")

        return replication_key

    def getLastSuccessfulRunReplicationKey(self, jobId, bookmark, start_date):
        replication_key = start_date
        jsonPayload = '{}'
        
        conn = None
        try:
            # DB Connection
            conn = psycopg2.connect(
                host=self.config["postgres_remote_host"],
                database=self.config["postgres_remote_database"],
                user=self.config["postgres_remote_user"],
                password=self.config["postgres_remote_password"]
            )

            # Create a cursor
            cur = conn.cursor()

            # Define the query
            query = f"""SELECT payload
                FROM runs
                WHERE
                job_name = '{jobId}' AND
                state = 'SUCCESS'
                ORDER BY id DESC
                LIMIT 1"""

            # Execute the query
            cur.execute(query)

            # Fetch the first row
            queryResult = cur.fetchone()

            if queryResult:
                jsonPayload = queryResult[0]

            # Close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            quit()
        finally:
            if conn is not None:
                conn.close()
                # print('Database connection closed.')

        dictPayload = json.loads(jsonPayload)

        # LOGGER.warning("::::::::::::::::::::::::::::::::::::::::::::")
        # LOGGER.warning(dictPayload)
        # LOGGER.warning("::::::::::::::::::::::::::::::::::::::::::::")

        if self.keys_exists(dictPayload, "singer_state", "bookmarks", bookmark, "replication_key_value"):
            replication_key = dictPayload['singer_state']['bookmarks'][bookmark]['replication_key_value']
            replication_key = cast(datetime, pendulum.parse(replication_key))

        return replication_key


    def keys_exists(self, element, *keys):
        '''Check if *keys (nested) exists in `element` (dict).'''
        if not isinstance(element, dict):
            raise AttributeError('keys_exists() expects dict as first argument.')
        if len(keys) == 0:
            raise AttributeError('keys_exists() expects at least two arguments, one given.')

        _element = element
        for key in keys:
            try:
                _element = _element[key]
            except KeyError:
                return False
        return True